package cloudcode.helloworld.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.google.cloud.tasks.v2.CloudTasksClient;
import com.google.cloud.tasks.v2.HttpMethod;
import com.google.cloud.tasks.v2.HttpRequest;
import com.google.cloud.tasks.v2.OidcToken;
import com.google.cloud.tasks.v2.QueueName;
import com.google.cloud.tasks.v2.Task;
import com.google.protobuf.ByteString;
import java.io.IOException;
import java.nio.charset.Charset;

/** Defines a controller to handle HTTP requests */
@Controller
public final class DefaultController {

  private static String project;
  private static final Logger logger = LoggerFactory.getLogger(HelloWorldController.class);

  /**
   * Create an endpoint for the landing page
   *
   * @return the index view template
   */
  /**
     *
     * @return String
     */
    @RequestMapping (path="/healthcheck", method= RequestMethod.GET)
    public ResponseEntity healthcheck()  {

    logger.info("start");
         // TODO(developer): Replace these variables before running the sample.
    String projectId = "mohanganesh";
    String locationId = "us-central1";
    String queueId = "task-123";
    String serviceAccountEmail =
        "sa-workflows@mohanganesh.iam.gserviceaccount.com";
  
        
  // Instantiates a client.
  try (CloudTasksClient client = CloudTasksClient.create()) {
    String url =
        "https://us-central1-mohanganesh.cloudfunctions.net/function-2"; // The full url path that the request will be sent to
    String payload = "Hello, World!"; // The task HTTP request body

    // Construct the fully qualified queue name.
    String queuePath = QueueName.of(projectId, locationId, queueId).toString();

    // Add your service account email to construct the OIDC token.
    // in order to add an authentication header to the request.
    OidcToken.Builder oidcTokenBuilder =
        OidcToken.newBuilder().setServiceAccountEmail(serviceAccountEmail);

    // Construct the task body.
    Task.Builder taskBuilder =
        Task.newBuilder()
            .setHttpRequest(
                HttpRequest.newBuilder()
                    .setBody(ByteString.copyFrom(payload, Charset.defaultCharset()))
                    .setHttpMethod(HttpMethod.POST)
                    .setUrl(url)
                    .setOidcToken(oidcTokenBuilder)
                    .build());

    // Send create task request.
    Task task = client.createTask(queuePath, taskBuilder.build());
    
    System.out.println("Task created: " + task.getName());
  } catch (Exception e) {
      e.printStackTrace();
  }
        return ResponseEntity.ok("alive");

    }

}
