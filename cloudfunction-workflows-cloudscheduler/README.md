
https://cloud.google.com/sdk/gcloud/reference/functions/deploy#--ingress-settings


gcloud functions deploy nodejs-http-function \
--gen2 \
--runtime=nodejs16 \
--region=us-central1 \
--source=. \
--entry-point=hello \
--trigger-http \
--ingress-settings=internal-only

gcloud functions describe nodejs-http-function --gen2 --region us-central1 --format="value(serviceConfig.uri)"


curl -m 70 -X POST https://nodejs-http-function-sn5rsq6dda-uc.a.run.app \
    -H "Authorization: bearer $(gcloud auth print-identity-token)" \
    -H "Content-Type: application/json" \
    -d '{}'




    gcloud services enable workflows.googleapis.com

    gcloud iam service-accounts create sa-workflows


    gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member "serviceAccount:sa-workflows@$PROJECT_ID.iam.gserviceaccount.com" \
    --role "roles/logging.logWriter"



    gcloud workflows deploy sample-glue --source=sample.yaml \
    --service-account=sa-workflows@$PROJECT_ID.iam.gserviceaccount.com

gcloud workflows run sample-glue


cloud schduler


  gcloud projects add-iam-policy-binding $PROJECT_ID \
      --member "serviceAccount:sa-workflows@$PROJECT_ID.iam.gserviceaccount.com" \
      --role "roles/workflows.invoker"


  gcloud projects add-iam-policy-binding $PROJECT_ID \
      --member "serviceAccount:sa-workflows@$PROJECT_ID.iam.gserviceaccount.com" \
      --role "roles/logging.logWriter"



gcloud scheduler jobs create http my-workflow-job \
    --schedule="*/5 * * * *" \
    --uri="https://workflowexecutions.googleapis.com/v1/projects/mohanganesh/locations/us-central1/workflows/sample-glue/executions" \
    --message-body="{\"argument\": \"{\\\"firstName\\\":\\\"Sherlock\\\", \\\"lastName\\\":\\\"Holmes\\\"}\"}" \
    --time-zone="America/Los_Angeles" \
    --oauth-service-account-email="sa-workflows@$PROJECT_ID.iam.gserviceaccount.com"

    
